package giocoDaTavolo2;


public class GameExecuter {
      
	public void executeGame(Player p1, Player p2, Gioco g) {
		g.addPlayer(p1);
		g.addPlayer(p2);
		g.buildMap();
		
		try {
		while (!g.gameOver()) {
			if (g.currentPlayer().equals(p1)) {
				String s1 = p1.move();
				if (g.validMove(p1, s1)) {
				  g.executeMove(p1, s1);	
				} else {
					System.out.println("Mossa non valida! " + p1.getName() + " ha perso il turno!");
					g.switchPlayer();
					continue;			
			      } 
			}
			
			if (g.currentPlayer().equals(p2) && !g.gameOver()) {
				String s2 = p2.move();
				if (g.validMove(p2, s2)) {
				  g.executeMove(p2, s2);	
				} else {
					System.out.println("Mossa non valida! " + p2.getName() + " ha perso il turno!");
					g.switchPlayer();
					continue;			
			      } 
			}
		}
		System.out.println( "Game Over!");
		g.getWinner(); 
		System.out.println("Il vincitore � " + g.getWinner().getName());
		} catch (NullPointerException e) {
		System.out.println("Pareggio!");
		
		}

		 
	}
}
