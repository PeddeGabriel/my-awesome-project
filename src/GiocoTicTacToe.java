package giocoDaTavolo2;



import java.util.HashMap;
import java.util.Map;

public class GiocoTicTacToe implements Gioco {
    private Map<Integer, String> mappa;
    private Player[] giocatori;
    private Player currentPlayer;
	private boolean pareggio;
    public enum Numbers {
    	
    }
    public GiocoTicTacToe() {
    	mappa = new HashMap<Integer, String>();
    	giocatori = new Player[2];
    	currentPlayer = null;
    	pareggio = false;
    	for (int i = 1; i < 10; i++) {
    		mappa.put(i, " ");
    	}
    }
	public void buildMap() {
      for (int j = 1; j <  8; j+=3) {
    	  System.out.print("[" + mappa.get(j) + "]");
    	  System.out.print("[" + mappa.get(j+1) + "]");
    	  System.out.print("[" + mappa.get(j+2) + "]");
    	  System.out.println();
      }
	}

	@Override
	public boolean addPlayer(Player p) {
		if (giocatori[0] == null) {
	    	  giocatori[0] = p;
	    	  System.out.println("Il giocatore 1 � " + p.getName() );
	    	  currentPlayer = p;
	    	  return true;
	      }
	      if (giocatori[1] == null) {
	    	  giocatori[1] = p;
	    	  System.out.println("Il giocatore 2 � " + p.getName() );
	    	  return true;
	      }
	        System.out.println("Ci sono gi� 2 giocatori!");
			return false;
	}

	@Override
	public boolean validMove(Player p, String s) {
		if ( Integer.parseInt(s) < 10 && p.equals(currentPlayer) && mappa.get(Integer.parseInt(s)).equals(" ") ) {
			return true;
		} else 
		return false;
	}

	@Override
	public void executeMove(Player p, String s) {
		if (validMove(p, s)) {
			if (p.equals(giocatori[0])) {
			    mappa.put(Integer.parseInt(s), "X");
			    buildMap();
			    if (!gameOver()) {
			    switchPlayer();
			    System.out.println("� il turno di " + currentPlayer.getName());
			    }
			}
			
			if (p.equals(giocatori[1])) {
				mappa.put(Integer.parseInt(s), "O");
				buildMap();
				if (!gameOver()) {
				    switchPlayer();
				    System.out.println("� il turno di " + currentPlayer.getName());
			   }
			}
		} 
		
	}

	@Override
	public boolean gameOver() {
		String s1 = mappa.get(1).trim();
		String s2 = mappa.get(2).trim();
		String s3 = mappa.get(3).trim();
		String s4 = mappa.get(4).trim();
		String s5 = mappa.get(5).trim();
		String s6 = mappa.get(6).trim();
		String s7 = mappa.get(7).trim();
		String s8 = mappa.get(8).trim();
		String s9 = mappa.get(9).trim();
		int j = 0;
		for (Integer i: mappa.keySet()) {
			if (mappa.get(i) != " ") {
				j++;
			}
		}
		if (s1.equals(s2) && s1.equals(s3) && !s1.equals("") || s4.equals(s5) && s4.equals(s6)  && !s4.equals("") || s7.equals(s8) && s7.equals(s9) && !s7.equals("") ){
			return true;
		}
		if (s1.equals(s4) && s1.equals(s7) && !s1.equals("") || s2.equals(s5) && s2.equals(s8) && !s2.equals("") || s3.equals(s6) && s3.equals(s9) && !s3.equals("") ){
			return true;
		}
		if (s1.equals(s5) && s1.equals(s9) && !s1.equals("") || s3.equals(s5) && s3.equals(s7) && !s3.equals("") ) {
			return true;
		}
		if ( j == 9) {
			pareggio = true;
			return true;
		   } 
			return false;
	}

	@Override
	public Player currentPlayer() {
		return currentPlayer;
	}

	@Override
	public boolean switchPlayer() {
		if (giocatori[0].equals(currentPlayer)) {
			currentPlayer = giocatori[1];
			return true;
		}
		if (giocatori[1].equals(currentPlayer)) {
			currentPlayer = giocatori[0];
			return true;
		}
		System.out.println("Manca un giocatore!");
		return false;
	}

	@Override
	public Player getWinner() throws NullPointerException {
	  	if (gameOver() && !pareggio) {
			return currentPlayer;
			}
			return null;
	}

}
