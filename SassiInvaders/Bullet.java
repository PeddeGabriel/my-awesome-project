/**
 * File by Marco L. Della Vedova <marco.dellavedova@unicatt.it>
 * for the course "Programmazione ad Oggetti", ed. 2019,
 * at the Dept. of Mathematics and Physics, 
 * Università Cattolica del Sacro Cuore, Brescia, Italy.
 */
package SassiInvaders;

import java.awt.Point;
import java.awt.geom.Point2D;
@SuppressWarnings("serial")
public class Bullet extends Point {
	private final Point2D initialPosition; 
	private Point2D position;
	private Point2D speed; 
	private double range;
    private final double theta;
	
	public Bullet(double x, double y, double theta) {
		this.theta = theta;
		initialPosition = new Point2D.Double(x, y);
		position = (Point2D) initialPosition.clone();
		speed = new Point2D.Double(6*Math.cos(theta), 6*Math.sin(theta));
		range = 300;

	}
	
	public void update() {
		double x = position.getX() + speed.getX();
		double y = position.getY() + speed.getY();
		position.setLocation(new Point2D.Double(x, y));
	}
	
	public boolean isActive() {
		return position.distance(initialPosition) < range;
	}
	public double getDirection() {
		return theta;
	}
	
	public void destroy() {
		position.setLocation(-range, 0);
	}

	public Point2D getPosition() {
		return position;
	}
	
}
