package giocoDaTavolo2;

import java.util.Random;

public class PlayerRobot implements Player {
    private String name;
    
    public PlayerRobot() {
  
    }
 	@Override
	public String move() {
	    Random ran = new Random();
	    int c = ran.nextInt(9) + 1;
	    String s = "" + c;
		return s;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
		
	}

}
