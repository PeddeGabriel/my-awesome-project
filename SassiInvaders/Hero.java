package SassiInvaders;

import java.awt.Point;
import java.awt.geom.Point2D;

public class Hero  {

	private final Point initialPosition; 
	private Point position;
	private final double speed; 
	private double hp;
	private final double maxhp;
	private boolean isAlive;
	
	public Hero(int x, int y, int speed, double maxhp) {
		initialPosition = new Point(x, y);
		position = (Point) initialPosition.clone();
		this.speed = speed;
		this.maxhp = maxhp;
		this.hp = maxhp;
		isAlive = true;
	}
	
	public Point2D getPosition() {
		return position;
	}
	public void move(int x, int y) {
		position.translate(x , y);
	}
	
	public double getSpeed() {
		return speed;
	}
	public double getMaxhp() {
		return maxhp;
	}
	public boolean isAlive() {
		if (hp == 0) 
			isAlive = false;
		return isAlive;
	}
	public double gethp() {
		return hp;
	}
	public void addhp(double d) {
		hp += d;
		if (hp > maxhp)
			hp = maxhp;
		if (hp < 0)
			hp = 0;
	}
}
