/**
 * File by Marco L. Della Vedova <marco.dellavedova@unicatt.it>
 * for the course "Programmazione ad Oggetti", ed. 2019,
 * at the Dept. of Mathematics and Physics, 
 * Università Cattolica del Sacro Cuore, Brescia, Italy.
 */
package SassiInvaders;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.Timer;

public class Controller  {
	
	private int fps = 30;
	
     World world;
	 WorldView view;
	 Random random;
	 Timer timer;
	 KeyListener keyListener;
	 ActionListener timerListener;
	 List<String> keyPressed;
	 MouseListener mouseListener;
	 MouseMotionListener mMotionListener;
	 private int t;
	 
	public Controller(World world, WorldView view) {
		this.world = world;
		this.view = view;
		this.random = new Random();
        
		t = 0;
		keyPressed = new ArrayList<String>();
		keyListener = createKeyListener();
		timerListener = createTimerListener(); 
		mouseListener = createMouseListener();
		mMotionListener = createMouseMotionListener();
		
		this.timer = new Timer(1000/fps, timerListener);
		// l'intervallo temporale del timer � in ms, 1000 ms in questo caso.
	   //  � un timer gestito da swing.
		// per come � stato scritto genera un evento (di tipo timer) ogni 1000/ fps ms,
		// per fps = 25, ogni 40ms.
		
		view.addMouseListener(mouseListener);
		view.addMouseMotionListener(mMotionListener);
	}

	private KeyListener createKeyListener() {
		return new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				keyPressed.remove(KeyEvent.getKeyText(e.getExtendedKeyCode()));		
//				if ( e.getExtendedKeyCode() == KeyEvent.VK_X ) {
//					if ( world.getStones().size() > 0 ) {
//						int r = random.nextInt(world.getStones().size());
//						world.getStones().remove(r);
//						world.notifyViews();
//				}
//				}
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if (!keyPressed.contains(KeyEvent.getKeyText(e.getExtendedKeyCode())))
				   keyPressed.add(KeyEvent.getKeyText(e.getExtendedKeyCode()));
				String dir = "";
				for (String j: keyPressed) {
					if (!dir.contains(j))
			              dir += j;		
				}
				switch (dir) {			
				case 	"D": world.getShip().move(10, 0);
				    break;
				case 	"W" : world.getShip().move(0, -10);
					break;
				case 	"S" : world.getShip().move(0, 10);
				    break;
				case 	"A" : world.getShip().move(-10, 0);
					break;
				case 	"DW" : case 	"WD" : world.getShip().move(10, -10);
					break;
				case 	"DS" : case 	"SD" : world.getShip().move(10, 10);
					break;
				case	"WA" : case		"AW" : world.getShip().move(-10, -10);
					break;
				case	"SA" : case 	"AS" : world.getShip().move(-10, 10);
					break;
					
				}
				world.notifyViews();
			
			}
		};
	}
	
	private MouseListener createMouseListener() {
		return new MouseAdapter() {

			
			@Override
			public void mousePressed(MouseEvent e) {	
			 if (e.getButton() == MouseEvent.BUTTON1 && t >= 10) {	
				t = 0;
				double x = e.getX()-world.getShip().getPosition().getX();
				double theta = Math.atan((e.getY()-world.getShip().getPosition().getY())/x);
				if (x >= 0) {
					view.rotateHero(theta + Math.PI/2);  
				    world.fire(theta);
				}
				else {
					view.rotateHero(theta - Math.PI/2);
				    world.fire(Math.PI + theta); 
				}
			 }
			 
			 if (e.getButton() == MouseEvent.BUTTON3) {
					double x = e.getX() - world.getShip().getPosition().getX();
			     	double y = e.getY() - world.getShip().getPosition().getY();
			     	double theta =  Math.atan(y/x);
			     	if (x < 0)
			     		world.addStone( e.getX(), e.getY(), theta );
			     	else
			     		world.addStone( e.getX(), e.getY(), theta + Math.PI );
				}
			}	
		};
	}
	
	private MouseMotionListener createMouseMotionListener() {
		return new MouseMotionAdapter() {

			@Override
			public void mouseMoved(MouseEvent e) {			
					double x = e.getX()- world.getShip().getPosition().getX();
					double theta = Math.atan((e.getY()-world.getShip().getPosition().getY())/x);
					if (x >= 0) {
						view.rotateHero(theta + Math.PI/2);  
					}
					else {
						view.rotateHero(theta - Math.PI/2);
			        }
	       }
			
			@Override
			public void mouseDragged(MouseEvent e) {			
					mouseMoved(e);
	       }
	   };
	}
	private ActionListener createTimerListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (t < 10)
				       t++;
				world.update();		
				// a cadenza regolare, il timer aggiorna il mondo.
			}
		};
	}
	
	public void start() {
		timer.start();
	}
	
}
