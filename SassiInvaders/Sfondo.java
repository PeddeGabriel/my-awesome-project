package SassiInvaders;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JLabel;
@SuppressWarnings("serial")
public class Sfondo extends JLabel {

	private BufferedImage stars;
	
	public Sfondo() throws IOException {
		
		this.stars = ImageIO.read(getClass().getResource("res/back.png"));
		repaint();
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
        int h = this.getHeight();
        int w = this.getWidth();
        g.drawImage(stars.getScaledInstance(w, h, 0), 0, 0,
				null);
	}
}
