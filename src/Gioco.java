package giocoDaTavolo2;



public interface Gioco {
	void buildMap();
    boolean addPlayer(Player p);
    boolean validMove(Player p, String s);
    void executeMove(Player p, String s);
    boolean gameOver();
    Player currentPlayer();
    boolean switchPlayer();
    Player getWinner();
}
