package SassiInvaders;

import java.awt.Point;
import java.awt.geom.Point2D;

@SuppressWarnings("serial")
public class Stone extends Point {

	private final Point2D initialPosition; 
	private Point2D position;
	private Point2D speed; 
    private boolean isDestroyed;
	
	public Stone(double x, double y, double theta) {
		initialPosition = new Point2D.Double(x, y);
		position = (Point2D) initialPosition.clone();
		speed = new Point2D.Double(6*Math.cos(theta), 6*Math.sin(theta));
        isDestroyed = false;
	}
	
	public void update() {
		double x = position.getX() + speed.getX();
		double y = position.getY() + speed.getY();
		position.setLocation(new Point2D.Double(x, y));
	}
	
	public void destroy() {
		isDestroyed = true;
	}

	public boolean isDestroyed() {
		return isDestroyed;
	}
	
	public Point2D getPosition() {
		return position;
	}
	
}
