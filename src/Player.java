package giocoDaTavolo2;

public interface Player {
    String move();
    String getName();
    void setName(String name);
    
}
