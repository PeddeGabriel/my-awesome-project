package giocoDaTavolo2;

import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class GiocoDiNim implements Gioco {
    private Queue<String> fiammiferi;
    private Player[] giocatori;
    private Player currentPlayer;
    
    public GiocoDiNim(int fiammiferi) {
    	this.fiammiferi = new ArrayBlockingQueue<String>(fiammiferi);
    	giocatori = new Player[2];
    }
	@Override
	public void buildMap() {
		try {
			while (true) {
			fiammiferi.add("|");
			}
		} catch (IllegalStateException e) {
			
		} finally {
		System.out.println(fiammiferi);
		}
	}

	@Override
	public boolean addPlayer(Player p) {
      if (giocatori[0] == null) {
    	  giocatori[0] = p;
    	  System.out.println("Il giocatore 1 � " + p.getName() );
    	  currentPlayer = p;
    	  return true;
      }
      if (giocatori[1] == null) {
    	  giocatori[1] = p;
    	  System.out.println("Il giocatore 2 � " + p.getName() );
    	  return true;
      }
        System.out.println("Ci sono gi� 2 giocatori!");
		return false;
	}
    
	public boolean switchPlayer() {
		if (giocatori[0].equals(currentPlayer)) {
			currentPlayer = giocatori[1];
			return true;
		}
		if (giocatori[1].equals(currentPlayer)) {
			currentPlayer = giocatori[0];
			return true;
		}
		System.out.println("Manca un giocatore!");
		return false;
	}
	@Override
	public boolean validMove(Player p, String s) {
		if ( Integer.parseInt(s) < 10 && p.equals(currentPlayer)) {
			return true;
		} else 
		return false;
	}

	@Override
	public void executeMove(Player p, String s) {
	   if (validMove(p,s)) {
		   	int i = (int) Integer.parseInt(s);
		   	for (int j = 0; j < i; j++) { try {
		   		fiammiferi.remove();
		   	    } catch (NoSuchElementException e) {
		   	    	 gameOver();
		   	    	 break;
		   	       }
		   	} if (!fiammiferi.isEmpty()) {
		   	System.out.println(fiammiferi);
		   	switchPlayer();
		   	System.out.println("� il turno di: " + currentPlayer.getName());
		   	}
	   } else {
		   System.out.println("Mossa non valida!");
	   }
	}

	@Override
	public boolean gameOver() {
	   if (fiammiferi.isEmpty()) {
		return true;
	   } 
		return false;
	}

	@Override
	public Player currentPlayer() {
		return currentPlayer;
	}

	@Override
	public Player getWinner() {
		if (gameOver()) {
		return currentPlayer;
		}
		return null;
	}

}
