package SassiInvaders;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class WorldView extends JPanel implements IView {
	
	private World world;	
	private ImageLoader il;
	private double theta;
	public WorldView(World world) {
		this.world = world;
		try {
			il = new ImageLoader();
	
		} catch (IOException e) {
			e.printStackTrace();
		}
		theta = 0;
		setPreferredSize(new Dimension(800, 600));
		setBackground(Color.WHITE);
	}
	
		
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
//		for ( Point2D p : world.getPoints() ) {
//			drawPoint((Graphics2D)g, p);
//		}
		for ( Stone s : world.getStones() ) {
			drawStone((Graphics2D)g, s.getPosition());
		}
		for ( Bullet b : world.getBullets() ) {		
				drawBullet((Graphics2D)g, b.getPosition(), b.getDirection());
		}
		drawHero((Graphics2D)g, world.getShip().getPosition(), theta);
	}
	
//	private void drawPoint(Graphics2D g, Point2D p) {
//		g.setColor(Color.BLUE);
//		g.fillOval((int)p.getX()-10, (int)p.getY()-10, 20, 20);
//		g.setColor(Color.MAGENTA);
//		g.fillRect((int)p.getX(), (int)p.getY(), 10, 10);
//	}
	
	private void drawBullet(Graphics2D g, Point2D p, double theta) {
		int imgDim = 30;	
			g.rotate(theta + Math.PI/2, p.getX() , p.getY());
			g.drawImage(il.getMissile().getScaledInstance(imgDim , imgDim + 10 , 0),
					(int)p.getX() - 10, 
					(int)p.getY() - 5,
					null);
			g.rotate(-theta - Math.PI/2, p.getX() , p.getY());	
	}
	
	
	private void drawStone(Graphics2D g, Point2D p) {
		int imgDim = 50;
		g.drawImage(il.getStoneImg().getScaledInstance(imgDim , imgDim , 0),
				(int)p.getX() - imgDim/2, 
				(int)p.getY() - imgDim/2,
				null);
	}
	
	private void drawHero(Graphics2D g, Point2D p, double theta) {
		int imgDim = 50;
		g.rotate(theta, p.getX(),p.getY());
		g.drawImage(il.getShipImg().getScaledInstance(imgDim, imgDim, 0),
				(int)p.getX() - imgDim/2, 
				(int)p.getY() - imgDim/2,
				this);
        g.rotate(-theta, p.getX(),p.getY());
	}
	
	public void rotateHero(double theta) {
		this.theta = theta;
	}

	@Override
	public void update() {
		repaint();
	}
}
