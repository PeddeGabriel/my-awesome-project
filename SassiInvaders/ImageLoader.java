package SassiInvaders;

import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;

public class ImageLoader {

	private BufferedImage stoneImg;
	private BufferedImage shipImg;
	private BufferedImage missile;
	
	public ImageLoader() throws IOException {
		this.stoneImg = ImageIO.read(getClass().getResource("res/Rock_Pile.png"));
		this.shipImg = ImageIO.read(getClass().getResource("res/ship.png"));
		this.missile = ImageIO.read(getClass().getResource("res/missile00.png"));
	}
	
	public BufferedImage getStoneImg() {
		return stoneImg;
	}

	public BufferedImage getShipImg() {
		return shipImg;
	}

	public BufferedImage getMissile() {
		return missile;
	}
	
}
