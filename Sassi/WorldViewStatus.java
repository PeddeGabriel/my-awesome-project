package lez14;

import java.awt.Graphics;

import javax.swing.JLabel;
@SuppressWarnings("serial")
public class WorldViewStatus extends JLabel {

	private World world;
	public WorldViewStatus(World world) {
		this.world = world;
		
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int np = world.getPoints().size();
		setText(String.valueOf(np));
	}
}
