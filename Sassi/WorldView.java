package lez14;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class WorldView extends JPanel {

	private World world;
	
	private BufferedImage stoneImg;
	
	public WorldView(World world) {
		this.world = world;
		loadImages();
		setPreferredSize(new Dimension(800, 600));
	}
	
	private void loadImages() {
	  try {
		this.stoneImg = ImageIO.read(getClass().getResource("Rock_Pile.png"));
	} catch (IOException e) { //il getResource ci aiuta quando dobbiamo caricare file che fanno parte
		                      // del programma, indipendentemente dalla posizione del programma
		    // in tal modo il file path non � dipendente dalla directory in cui lancio il programma
		//cos� anche cambiando pc, il programma funziona ugualmente
		e.printStackTrace();
	  }	
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		for (Point2D p: world.getPoints()) {
			drawPoint((Graphics2D) g, p);
		}
		for (Point2D p: world.getStones()) {
			drawStone((Graphics2D) g, p);
		}
	}
	
	private void drawPoint(Graphics2D g, Point2D p) {
		g.setColor(Color.GREEN);
		g.fillOval((int) p.getX()-10, (int)p.getY()-10, 20, 20);
		g.setColor(Color.magenta);
		g.fillRect((int) p.getX()-5, (int) p.getY()-5, 10, 10);
	}
	
	private void drawStone(Graphics2D g, Point2D p) {
		int imgDim = 50;
		g.drawImage(stoneImg.getScaledInstance(imgDim,  imgDim, 0),
				(int) p.getX() - imgDim/2, (int) p.getY() - imgDim/2, null);
		
	}
}
