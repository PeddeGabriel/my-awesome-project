package lez14;

import java.util.ArrayList;
import java.util.List;
import java.awt.geom.Point2D;
public class World {

	private List<Point2D> points; 
	private List<Point2D> stones;
	
	public World() {
		points = new ArrayList<Point2D>();
		stones = new ArrayList<Point2D>();
	}
	
	public void addStone(double x, double y) {
		stones.add(new Point2D.Double(x, y));
		
	}
	public void addPoint(double x, double y) {
		points.add(new Point2D.Double(x , y));		
	}
	
	public List<Point2D> getPoints() {
		return points;
	}
	
	public List<Point2D> getStones() {
		return stones;
	}
	
}
