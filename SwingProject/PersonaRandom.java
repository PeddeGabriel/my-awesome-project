package lez12;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class PersonaRandom {
    
	private static List<String> nomi;
	private static JFrame frame;
	private static JLabel label; 
	private static JButton button;
	private static Random ran;
	
	static {
		frame = new JFrame("Persona Random");
		label = new JLabel("RPG");
		button = new JButton("Premimi");
		ran = new Random();
		nomi = new ArrayList<String>();	
	}
	
	private static void assembleFrame(JFrame frame, JLabel label, JButton button) {
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(label, BorderLayout.CENTER);
		frame.getContentPane().add(button, BorderLayout.SOUTH);
		frame.setSize(400, 200);
		frame.setLocation(620, 260);  
		
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setOpaque(true);
		label.setBackground(Color.PINK);
		label.setFont(new Font(Font.SERIF, Font.ITALIC, 62));
			
	}
	
	private static void showFrame(JFrame frame) {
		frame.setVisible(true);
	}
	
	private static void addActionToButton(JButton button, List<String> nomiA) {
	
		int n = nomiA.size();
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int k = ran.nextInt(n);
				label.setText(nomiA.get(k));
			}
		});
		
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		
		assembleFrame(frame, label, button);
		
		String fileString = "src/lez12/ListaNomi.txt";
		File file = new File(fileString);
		Scanner sc = new Scanner(file);
		sc.useDelimiter(" ");
		while (sc.hasNext()) {
			nomi.add(sc.next());
		}
	
		addActionToButton(button, nomi);
			
		sc.close();
		showFrame(frame);
	}
}
