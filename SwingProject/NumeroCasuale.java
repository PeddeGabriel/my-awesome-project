package lez12;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class NumeroCasuale {

	public static void main(String[] args) {
		
	JFrame frame = new JFrame("Numero casuale");
	JLabel l1 = new JLabel("Hi, I'm a GUI");
	JButton button = new JButton("Premimi");
	
	Random ran = new Random();
	frame.getContentPane().add(l1, BorderLayout.CENTER);
	frame.add(button, BorderLayout.SOUTH);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	frame.pack();
	frame.setSize(300, 200);
	frame.setLocation(620, 260);                //Da ricordare: classe Point
	l1.setHorizontalAlignment(SwingConstants.CENTER);
	l1.setOpaque(true);
	l1.setBackground(Color.PINK);
	l1.setFont(new Font("Serif", 0, 42));
	button.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			int n = ran.nextInt(14) + 1;
			l1.setText("" + n);
		}
	});
	frame.setVisible(true);
	
	}
	
}
