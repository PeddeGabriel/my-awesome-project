package lez12;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//awt � la classe padre di swing, quindi ho bisogno di classi anche da esso.
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class SwingDemo1 {

	public static void main(String[] args) {
	   
		JFrame frame = new JFrame("Esempio titolo finestra"); //creo un nuovo oggetto della componente "finestra"
        frame.setVisible(true);  // la rendo visibile quando eseguo il programma.
        //Di base il layout della finestra corrisponde con quello del sistema operativo.
	    // se chiudo la finestra, di default viene impostata la visibilit� a "False",
        // ma il programma non si interrompe.
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // in questo modo il programma � interrotto
        //quando chiudo la finestra.
        JLabel l1 = new JLabel();
        l1.setText("Hello World, I'm a GUI!");
        JButton button = new JButton("Non premermi");
        frame.getContentPane().add(l1, BorderLayout.CENTER);
        frame.getContentPane().add(button, BorderLayout.SOUTH);
        frame.pack();
        //Cambio colore finestra, allineo label al centro, cambio carattere.
        l1.setOpaque(true);
        l1.setBackground(Color.ORANGE);
        l1.setHorizontalAlignment(SwingConstants.CENTER);
        l1.setFont(new Font("Serif", Font.PLAIN, 42));
        button.addActionListener(new ActionListener() {
       //il button "genera l'evento" e lo fa ascoltare all'oggetto della classe "ActionListener()"
			@Override
			public void actionPerformed(ActionEvent e) {
				l1.setText("Ecco, hai premuto!");
				l1.setBackground(Color.RED);
				System.out.println("Button premuto!");
				
			}
	    });
        /*es.1 Fai un programma utile a estrarre un numero per un interrogazione in classe;
        un numero da 1 a 14;
        es.2 Fai leggere un elenco di 12 persone da un file e premendo il pulsante fai visualizzare una
        persona casuale dall'elenco
        Tutto ci� in una nuova classe.
        */
	}

}
