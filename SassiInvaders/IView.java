/**
 * File by Marco L. Della Vedova <marco.dellavedova@unicatt.it>
 * for the course "Programmazione ad Oggetti", ed. 2019,
 * at the Dept. of Mathematics and Physics, 
 * Università Cattolica del Sacro Cuore, Brescia, Italy.
 */
package SassiInvaders;

public interface IView {
	void update();
}
