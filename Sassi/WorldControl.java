package sassi;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.util.List;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;


public class WorldControl {

	private World world;
	private WorldView wview;
	private WorldViewStatus wvstatus;
	private JFrame frame;

	
	public WorldControl(World world, WorldView wview, WorldViewStatus wvstatus) {
		this.world = world;
		this.wview = wview;
		this.wvstatus = wvstatus;
		frame = new JFrame("Simple World");
		
		addListener();
		createWorld();
	}
	
	public void createWorld() {
	   	
	   frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	   frame.setBounds(350, 150, 100, 100);
	   JPanel panel = new JPanel();
	   frame.getContentPane().add(panel);
	   panel.setLayout(new BorderLayout());
	   panel.add(wview, BorderLayout.CENTER);
	   panel.add(wvstatus, BorderLayout.SOUTH);
	   wvstatus.setFont(new Font("TimesNewRoman", Font.PLAIN, 42));
	   wvstatus.setHorizontalAlignment(SwingConstants.CENTER);
	   frame.pack();
	   frame.setVisible(true);
	 
	}
	
	public void addListener() {
		wview.addMouseListener(new MouseAdapter() {
            	@Override
			public void mouseClicked(MouseEvent e) {
              if (e.getButton() == MouseEvent.BUTTON1)
                 world.addPoint(e.getX(), e.getY());
              if (e.getButton() == MouseEvent.BUTTON3)
            	 world.addStone(e.getX(), e.getY());
              wview.repaint();
              wvstatus.repaint();
			}
		});
		   
		frame.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				super.keyPressed(e);
				if (e.getKeyCode() == KeyEvent.VK_X) {
                    List<Point2D> stones = world.getStones();
				    int k = stones.size();
				    if (!stones.isEmpty())
			          stones.remove(new Random().nextInt(k));
				    wview.repaint();
				    wvstatus.repaint();
			   }
			}
     	});
	
	}
	
}
