package giocoDaTavolo2;

import java.util.Scanner;

public class PlayerUmano implements Player {
   private String name;
   private Scanner sc;
   
   public PlayerUmano() {
	   sc = new Scanner(System.in);
   }
	@Override
	public String move() {
		String move = sc.next();
		return move;
	}

	@Override
	public String getName() {
	     return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

}
