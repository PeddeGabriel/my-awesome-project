package SassiInvaders;

import java.awt.Graphics;

import javax.swing.JLabel;

@SuppressWarnings("serial")
public class WorldViewStatus extends JLabel implements IView {
	
	private World world;
	
	public WorldViewStatus(World world) {
		this.world = world;
		setText("0");
	}
	
	@Override
	protected void paintComponent(Graphics arg0) {
		super.paintComponent(arg0);
		int np = world.getPoints().size();
		int ns = world.getStones().size();
		setText(String.format("Points: %d, Stones: %d", np, ns));
	}

	@Override
	public void update() {
		repaint();		
	}

}
