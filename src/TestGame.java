package giocoDaTavolo2;

public class TestGame {

	 public static void main(String[] args) {
		 Gioco giocoDiNim = new GiocoDiNim(30);
		 Player playerUmano = new PlayerUmano();
		 Player playerRobot = new PlayerRobot();
		 Player playerUmano2 = new PlayerUmano();
		 playerUmano.setName("Marco");
		 playerRobot.setName("Hal");
		 playerUmano2.setName("George");
	      
		 GameExecuter gameExe = new GameExecuter();
		 gameExe.executeGame(playerUmano, playerRobot, giocoDiNim); 
		 
		 Gioco ticTacToe = new GiocoTicTacToe();
		 gameExe.executeGame(playerUmano, playerRobot, ticTacToe);
		 
	 }
}
