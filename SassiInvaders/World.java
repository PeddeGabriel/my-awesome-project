package SassiInvaders;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class World {
	
	private List<IView> views;
	
	private List<Point> points;
	private List<Stone> stones;
	private List<Bullet> bullets;
	private Hero ship;
	
	public World() {
		views = new ArrayList<IView>();
		points = new ArrayList<Point>();
		stones = new ArrayList<Stone>();
		bullets = new ArrayList<Bullet>();
		ship = new Hero(400, 250, 10, 3);
	}
	
	public void addView(IView view) {
		views.add(view);
	}
	
	protected void notifyViews() {
		for (IView view : views) {
			view.update();
		}
	}
	
	public void addPoint(int x, int y) {
		Point p = new Point(x, y);
		points.add(p);
		notifyViews();
	}
	
	public void addStone(double x, double y, double theta) {
		stones.add(new Stone(x, y, theta));
		notifyViews();
	}
	
	public Hero getShip() {
		return ship;
	}
	
	public List<Point> getPoints() {
		return points;
	}
	
	public List<Stone> getStones() {
		return stones;
	}
	
	public List<Bullet> getBullets() {
		return bullets;
	}
	
	public void update() {
		for ( Stone stone : getStones() ) {	
			for (Bullet bullet : getBullets())
				if (bullet.getPosition().distance(stone.getPosition()) < 30) {
					stone.destroy();
					bullet.destroy();
				}			   
		}
		stones.forEach(s -> s.update());
		bullets.forEach(b -> b.update());// significa: per ogni elemento b di bullets, esegui l'azione update
		
		clean();
		notifyViews();
	}
	
	public void fire(double theta) {
		double x = ship.getPosition().getX();
		double y = ship.getPosition().getY();
		Bullet b1 = new Bullet(x + 28*Math.sin(theta) , y   - 28*Math.cos(theta), theta);
		Bullet b2 = new Bullet(x  - 18*Math.sin(theta) , y  + 18*Math.cos(theta), theta);
		bullets.add(b1);
		bullets.add(b2);
		
	}
	
	public void clean() { //voglio rimuovere le rocce fuori dallo schermo per ottimizzare la memoria.
//	List<Point> toRemove = new ArrayList<Point>();
//	for ( Point s : getStones() ) {
//		if ( s.getX() < 0 ) {
//			toRemove.add(s);
//			}
//		}
//	stones.removeAll(toRemove);
	
    stones.removeIf(s -> (s.isDestroyed())); //� un metodo alternativo per dire in una riga ci� che prima compare
    //in pi� righe.
    // la funzione all'interno del removeIf � una "lambda function"
    // gli oggetti da rimuovere sono rappresentati da un predicato (boolean value) all'interno della
    // lambda function, rappresentante la condizione per cui rimuovere l'oggetto
    // Si legge: "prendi l'array stones e rimuovi gli oggetti per cui la coordinata x � minore di 0 (s.x < 0)
    
    bullets.removeIf(x -> ! x.isActive()); // svolge la medesima funzione della funzione precedente
    // la funzione � eseguita se � verificata la condizone, ovvero se il bullet x non � attivo
	}

}
