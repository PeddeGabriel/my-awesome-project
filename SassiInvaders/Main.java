package SassiInvaders;

import java.awt.BorderLayout;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.SwingConstants;

public class Main {

	public static void main(String[] args) throws IOException {
		World world = new World();
		WorldView wview = new WorldView(world);
		WorldViewStatus ws = new WorldViewStatus(world);
		Controller controller = new Controller(world, wview);

		
		world.addView(wview);
		world.addView(ws);
		JFrame frame = new JFrame("Simple world MVC");
		frame.setBounds(500, 200, 10, 10);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(wview);
		frame.add(ws, BorderLayout.SOUTH);
		frame.addKeyListener(controller.keyListener);
		frame.pack();
		frame.setVisible(true);
		controller.start();
	
		/* per la prossima volta: 
		 * - sparare nella direzione del mouse
		 * - mettere un' immagine al posto dell'eroe
		 */
	}

}
